PiHat Manufacturing Process on PCB

1. Step 1 – Printing the Design: Print the design onto PCB using a plotted printer. 
2. Step 2 – Create the Substrate: Pass the materials through an oven to be semicured to form the substrate. 
3. Step 3 – Printing the Inner Layers: Print to a laminate. Drill holes into PCB to help with alignment. 
4. Step 4 – Ultraviolet Light: Put under ultraviolet light to harden.
5. Step 5 – Removing Unwanted Copper: Use a chemical solution to remove unwanted copper.
6. Step 6 – Inspection: Ensure everything is aligned.
7. Step 7 – Laminating the Layers
8. Step 8 – Pressing the Layers: Use a mechanical press to press the layers. 
9. Step 9 – Drilling: Drill holes into the board using a computer-guided drill.
10. Step 10 – Plating: Plate the board using a chemical solution. 
11. Step 11 – Outer Layer Imaging: Use ultraviolight to harden photoresist.
12. Step 12 – Plating
13. Step 13 – Etching: Remove unwanted copper to establish PCB connections. 
14. Step 14 – Solder Mask Application: Clean panels before applying soldering mask. 
15. Step 15 – Silkscreening: Print critical information onto board. 
16. Step 16 – Surface Finish: Plate with solderable finish. 
17. Step 17 – Testing: Test board to make sure it operates as original design says it should. 
