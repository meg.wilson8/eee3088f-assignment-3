EESchema Schematic File Version 4
LIBS:UPSGROUP8-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Voltage Regulator Circuit"
Date "2021-06-04"
Rev "1"
Comp "UCT Group 9 "
Comment1 "Regulates to 5V"
Comment2 "When battery drops below 5V, circuit does not operate correctly"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LT3012EDEPBF:LT3012EDEPBF IC1
U 1 1 60B92531
P 4400 3200
F 0 "IC1" H 5000 3465 50  0000 C CNN
F 1 "LT3012EDEPBF" H 5000 3374 50  0000 C CNN
F 2 "LT3012EDEPBF:SON50P300X400X80-13N-D" H 5450 3300 50  0001 L CNN
F 3 "https://www.mouser.in/datasheet/2/609/3012fd-1272073.pdf" H 5450 3200 50  0001 L CNN
F 4 "250mA, 4V to 80V Low Dropout Micropower Linear Regulator" H 5450 3100 50  0001 L CNN "Description"
F 5 "0.8" H 5450 3000 50  0001 L CNN "Height"
F 6 "Linear Technology" H 5450 2900 50  0001 L CNN "Manufacturer_Name"
F 7 "LT3012EDEPBF" H 5450 2800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5450 2700 50  0001 L CNN "Mouser Part Number"
F 9 "" H 5450 2600 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5450 2500 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5450 2400 50  0001 L CNN "Arrow Price/Stock"
	1    4400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3600 6300 3600
Wire Wire Line
	6300 3600 6300 3400
Wire Wire Line
	6300 3400 5600 3400
Wire Wire Line
	6300 3400 7000 3400
Connection ~ 6300 3400
$Comp
L Device:C_Small C1
U 1 1 60B9629A
P 3050 3450
F 0 "C1" H 3142 3496 50  0000 L CNN
F 1 "3.3u" H 3142 3405 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 3050 3450 50  0001 C CNN
F 3 "~" H 3050 3450 50  0001 C CNN
	1    3050 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 60B952FB
P 3550 3600
F 0 "R2" H 3618 3646 50  0000 L CNN
F 1 "249K" H 3618 3555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3550 3600 50  0001 C CNN
F 3 "~" H 3550 3600 50  0001 C CNN
	1    3550 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B944E3
P 3550 3400
F 0 "R1" H 3618 3446 50  0000 L CNN
F 1 "750K" H 3618 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3550 3400 50  0001 C CNN
F 3 "~" H 3550 3400 50  0001 C CNN
	1    3550 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3300 3550 3300
Wire Wire Line
	4400 3500 3550 3500
Connection ~ 3550 3500
Wire Wire Line
	3550 3300 3050 3300
Wire Wire Line
	3050 3300 3050 3350
Connection ~ 3550 3300
Wire Wire Line
	3550 3750 3550 3700
Wire Wire Line
	3050 3550 3050 3750
Connection ~ 3050 3750
Wire Wire Line
	3050 3750 3550 3750
Wire Wire Line
	3550 3750 4200 3750
Wire Wire Line
	4200 3750 4200 3600
Wire Wire Line
	4200 3600 4400 3600
Connection ~ 3550 3750
Wire Wire Line
	3050 3750 3050 4050
Text GLabel 3050 4050 0    50   Input ~ 0
GND
Text GLabel 7000 3400 0    50   Input ~ 0
Vbat
Text HLabel 3050 3300 0    50   Input ~ 0
Pout
NoConn ~ 5600 3200
NoConn ~ 5600 3300
NoConn ~ 5600 3500
NoConn ~ 4400 3200
NoConn ~ 4400 3400
NoConn ~ 4400 3700
NoConn ~ 5600 3700
NoConn ~ 5000 4200
$EndSCHEMATC
