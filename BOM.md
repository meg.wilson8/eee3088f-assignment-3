BILL OF MATERIALS:

-	10V Battery 
-	750kOhm Resistor 
-	249kOhm Resistor 
-	3.3uF Capacitor 
-	IC LT3012 Voltage Regulator 
-	3x10kOhm Resistor 
-	3.3kOhm Resistor 
-	6xIC OP1177 Op Amp
-	4kOhm Resistor 
-	6kOhm Resistor 
-	3kOhm Resistor 
-	7kOhm Resistor 
-	2kOhm Resistor 
-	8kOhm Resistor 
-	10kOhm Resistor 
-	680Ohm Resistor 
-	4 NSPW500BS LEDs
-	1N750 Zener Diode 

