Many users are setting up Pis as a home or media server, storage device along with a lot of other uses.The microhat will be used as an UPS (uninterrupted power supply) for the Pi. The HAT will keep a Pi operating during times when it’s power supply is cut. The HAT will have a voltmeter that will lead to the signal line between 0-3.3V so the user will be able to know when to shut down the Pi. The HAT also has an LED to warn the user when the battery is at >80%,70%<V<80%, 60%<V<70% and <60% (very low).

Subsystem 1: Power supply subsystem.
    - Regulates the battery voltage to 5V (note that when the battery voltage gets below 5V, it will no-longer output 5V)

Subsystem 2: Voltage amplifier.
    - Outputs voltage between 0V and 3.3V 

Subsystem 3: LED battery voltage indicator.
    - Has white status LEDs to indicate the battery level
    - 4 LEDs on when the voltage is between 8V and 10V
    - 3 LEDs on when the voltage is between 7V and 8V
    - 2 LEDs on when the voltage is between 6V and 7V
    - 1 LEDs on when the voltage is between 5V and 6V
