EESchema Schematic File Version 4
LIBS:UPSGROUP8-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Status LED Circuit"
Date "2021-06-04"
Rev "1"
Comp "UCT Group 9"
Comment1 "Decreasing battery power results in decreasing number of LEDs on"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC7
U 1 1 60B7F8A3
P 4500 2150
F 0 "IC7" H 5100 2415 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 5100 2324 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 5550 2250 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 5550 2150 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 5550 2050 50  0001 L CNN "Description"
F 5 "1.1" H 5550 1950 50  0001 L CNN "Height"
F 6 "Analog Devices" H 5550 1850 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 5550 1750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 5550 1650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 5550 1550 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 5550 1450 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 5550 1350 50  0001 L CNN "Arrow Price/Stock"
	1    4500 2150
	1    0    0    -1  
$EndComp
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC6
U 1 1 60B817C6
P 4500 3000
F 0 "IC6" H 5100 3265 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 5100 3174 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 5550 3100 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 5550 3000 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 5550 2900 50  0001 L CNN "Description"
F 5 "1.1" H 5550 2800 50  0001 L CNN "Height"
F 6 "Analog Devices" H 5550 2700 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 5550 2600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 5550 2500 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 5550 2400 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 5550 2300 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 5550 2200 50  0001 L CNN "Arrow Price/Stock"
	1    4500 3000
	1    0    0    -1  
$EndComp
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC5
U 1 1 60B822D7
P 4550 3850
F 0 "IC5" H 5150 4115 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 5150 4024 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 5600 3950 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 5600 3850 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 5600 3750 50  0001 L CNN "Description"
F 5 "1.1" H 5600 3650 50  0001 L CNN "Height"
F 6 "Analog Devices" H 5600 3550 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 5600 3450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 5600 3350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 5600 3250 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 5600 3150 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 5600 3050 50  0001 L CNN "Arrow Price/Stock"
	1    4550 3850
	1    0    0    -1  
$EndComp
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC4
U 1 1 60B8341A
P 4550 4700
F 0 "IC4" H 5150 4965 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 5150 4874 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 5600 4800 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 5600 4700 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 5600 4600 50  0001 L CNN "Description"
F 5 "1.1" H 5600 4500 50  0001 L CNN "Height"
F 6 "Analog Devices" H 5600 4400 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 5600 4300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 5600 4200 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 5600 4100 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 5600 4000 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 5600 3900 50  0001 L CNN "Arrow Price/Stock"
	1    4550 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R16
U 1 1 60B841FF
P 2650 2500
F 0 "R16" V 2855 2500 50  0000 C CNN
F 1 "4k" V 2764 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2650 2500 50  0001 C CNN
F 3 "~" H 2650 2500 50  0001 C CNN
	1    2650 2500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R14
U 1 1 60B85C37
P 2650 3350
F 0 "R14" V 2855 3350 50  0000 C CNN
F 1 "3k" V 2764 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2650 3350 50  0001 C CNN
F 3 "~" H 2650 3350 50  0001 C CNN
	1    2650 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R12
U 1 1 60B864FA
P 2700 4200
F 0 "R12" V 2905 4200 50  0000 C CNN
F 1 "2k" V 2814 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 2700 4200 50  0001 C CNN
F 3 "~" H 2700 4200 50  0001 C CNN
	1    2700 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R17
U 1 1 60B8A123
P 3700 2500
F 0 "R17" V 3905 2500 50  0000 C CNN
F 1 "6k" V 3814 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3700 2500 50  0001 C CNN
F 3 "~" H 3700 2500 50  0001 C CNN
	1    3700 2500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R15
U 1 1 60B8A8C3
P 3700 3350
F 0 "R15" V 3905 3350 50  0000 C CNN
F 1 "7k" V 3814 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3700 3350 50  0001 C CNN
F 3 "~" H 3700 3350 50  0001 C CNN
	1    3700 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R13
U 1 1 60B8ADFF
P 3700 4200
F 0 "R13" V 3905 4200 50  0000 C CNN
F 1 "8k" V 3814 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3700 4200 50  0001 C CNN
F 3 "~" H 3700 4200 50  0001 C CNN
	1    3700 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R11
U 1 1 60B8B771
P 3700 5050
F 0 "R11" V 3905 5050 50  0000 C CNN
F 1 "10k" V 3814 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3700 5050 50  0001 C CNN
F 3 "~" H 3700 5050 50  0001 C CNN
	1    3700 5050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R7
U 1 1 60B8BC7C
P 6450 4900
F 0 "R7" V 6655 4900 50  0000 C CNN
F 1 "680" V 6564 4900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6450 4900 50  0001 C CNN
F 3 "~" H 6450 4900 50  0001 C CNN
	1    6450 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R8
U 1 1 60B8C8BE
P 6450 4050
F 0 "R8" V 6655 4050 50  0000 C CNN
F 1 "680" V 6564 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6450 4050 50  0001 C CNN
F 3 "~" H 6450 4050 50  0001 C CNN
	1    6450 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R9
U 1 1 60B8CF39
P 6450 3200
F 0 "R9" V 6655 3200 50  0000 C CNN
F 1 "680" V 6564 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6450 3200 50  0001 C CNN
F 3 "~" H 6450 3200 50  0001 C CNN
	1    6450 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R10
U 1 1 60B8D6FA
P 6400 2350
F 0 "R10" V 6605 2350 50  0000 C CNN
F 1 "680" V 6514 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6400 2350 50  0001 C CNN
F 3 "~" H 6400 2350 50  0001 C CNN
	1    6400 2350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 3200 8850 3200
Wire Wire Line
	6550 4050 8850 4050
Wire Wire Line
	5750 4900 6350 4900
Wire Wire Line
	6550 4900 8800 4900
Wire Wire Line
	2750 2500 3150 2500
Wire Wire Line
	2750 3350 3150 3350
Wire Wire Line
	2800 4200 3150 4200
Wire Wire Line
	4500 2350 3150 2350
Wire Wire Line
	3150 2350 3150 2500
Connection ~ 3150 2500
Wire Wire Line
	3150 2500 3600 2500
Wire Wire Line
	4500 3200 3150 3200
Wire Wire Line
	3150 3200 3150 3350
Connection ~ 3150 3350
Wire Wire Line
	3150 3350 3600 3350
Wire Wire Line
	4550 4050 3150 4050
Connection ~ 3150 4200
Wire Wire Line
	3150 4200 3600 4200
Wire Wire Line
	4550 4900 3150 4900
Wire Wire Line
	3150 4900 3150 5050
Connection ~ 3150 5050
Wire Wire Line
	3150 5050 3600 5050
Wire Wire Line
	3150 4050 3150 4200
Wire Wire Line
	6500 2350 8850 2350
$Comp
L NSPW500BS:NSPW500BS LED4
U 1 1 60BBD09D
P 9450 2350
F 0 "LED4" H 9750 2717 50  0000 C CNN
F 1 "NSPW500BS" H 9750 2626 50  0000 C CNN
F 2 "NSPW500BS:NSPW500BS" H 9950 2500 50  0001 L BNN
F 3 "https://www.digchip.com/datasheets/parts/datasheet/1014/NSPW500BS-pdf.php" H 9950 2400 50  0001 L BNN
F 4 "4 V White LED Through Hole, Nichia NSPW500BS" H 9950 2300 50  0001 L BNN "Description"
F 5 "13.3" H 9950 2200 50  0001 L BNN "Height"
F 6 "NICHIA" H 9950 2100 50  0001 L BNN "Manufacturer_Name"
F 7 "NSPW500BS" H 9950 2000 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "" H 9950 1900 50  0001 L BNN "Mouser Part Number"
F 9 "" H 9950 1800 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 9950 1700 50  0001 L BNN "Arrow Part Number"
F 11 "" H 9950 1600 50  0001 L BNN "Arrow Price/Stock"
	1    9450 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5700 2350 6300 2350
Wire Wire Line
	5700 3200 6350 3200
Wire Wire Line
	5750 4050 6350 4050
$Comp
L NSPW500BS:NSPW500BS LED3
U 1 1 60BC8C74
P 9450 3200
F 0 "LED3" H 9750 3567 50  0000 C CNN
F 1 "NSPW500BS" H 9750 3476 50  0000 C CNN
F 2 "NSPW500BS:NSPW500BS" H 9950 3350 50  0001 L BNN
F 3 "https://www.digchip.com/datasheets/parts/datasheet/1014/NSPW500BS-pdf.php" H 9950 3250 50  0001 L BNN
F 4 "4 V White LED Through Hole, Nichia NSPW500BS" H 9950 3150 50  0001 L BNN "Description"
F 5 "13.3" H 9950 3050 50  0001 L BNN "Height"
F 6 "NICHIA" H 9950 2950 50  0001 L BNN "Manufacturer_Name"
F 7 "NSPW500BS" H 9950 2850 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "" H 9950 2750 50  0001 L BNN "Mouser Part Number"
F 9 "" H 9950 2650 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 9950 2550 50  0001 L BNN "Arrow Part Number"
F 11 "" H 9950 2450 50  0001 L BNN "Arrow Price/Stock"
	1    9450 3200
	-1   0    0    1   
$EndComp
$Comp
L NSPW500BS:NSPW500BS LED2
U 1 1 60BC9E08
P 9450 4050
F 0 "LED2" H 9750 4417 50  0000 C CNN
F 1 "NSPW500BS" H 9750 4326 50  0000 C CNN
F 2 "NSPW500BS:NSPW500BS" H 9950 4200 50  0001 L BNN
F 3 "https://www.digchip.com/datasheets/parts/datasheet/1014/NSPW500BS-pdf.php" H 9950 4100 50  0001 L BNN
F 4 "4 V White LED Through Hole, Nichia NSPW500BS" H 9950 4000 50  0001 L BNN "Description"
F 5 "13.3" H 9950 3900 50  0001 L BNN "Height"
F 6 "NICHIA" H 9950 3800 50  0001 L BNN "Manufacturer_Name"
F 7 "NSPW500BS" H 9950 3700 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "" H 9950 3600 50  0001 L BNN "Mouser Part Number"
F 9 "" H 9950 3500 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 9950 3400 50  0001 L BNN "Arrow Part Number"
F 11 "" H 9950 3300 50  0001 L BNN "Arrow Price/Stock"
	1    9450 4050
	-1   0    0    1   
$EndComp
$Comp
L NSPW500BS:NSPW500BS LED1
U 1 1 60BCB115
P 9400 4900
F 0 "LED1" H 9700 5267 50  0000 C CNN
F 1 "NSPW500BS" H 9700 5176 50  0000 C CNN
F 2 "NSPW500BS:NSPW500BS" H 9900 5050 50  0001 L BNN
F 3 "https://www.digchip.com/datasheets/parts/datasheet/1014/NSPW500BS-pdf.php" H 9900 4950 50  0001 L BNN
F 4 "4 V White LED Through Hole, Nichia NSPW500BS" H 9900 4850 50  0001 L BNN "Description"
F 5 "13.3" H 9900 4750 50  0001 L BNN "Height"
F 6 "NICHIA" H 9900 4650 50  0001 L BNN "Manufacturer_Name"
F 7 "NSPW500BS" H 9900 4550 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "" H 9900 4450 50  0001 L BNN "Mouser Part Number"
F 9 "" H 9900 4350 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 9900 4250 50  0001 L BNN "Arrow Part Number"
F 11 "" H 9900 4150 50  0001 L BNN "Arrow Price/Stock"
	1    9400 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 2350 9700 2350
Wire Wire Line
	9450 3200 9700 3200
Wire Wire Line
	9450 4050 9700 4050
Wire Wire Line
	9400 4900 9700 4900
Wire Wire Line
	3800 2500 3900 2500
Wire Wire Line
	3900 2500 3900 3350
Wire Wire Line
	3900 3350 3800 3350
Wire Wire Line
	3900 3350 3900 4200
Wire Wire Line
	3900 4200 3800 4200
Connection ~ 3900 3350
Wire Wire Line
	3900 4200 3900 5050
Wire Wire Line
	3900 5050 3800 5050
Connection ~ 3900 4200
Connection ~ 3900 5050
Wire Wire Line
	9700 2350 9700 3200
Connection ~ 9700 3200
Wire Wire Line
	9700 3200 9700 4050
Connection ~ 9700 4050
Wire Wire Line
	9700 4050 9700 4900
Connection ~ 9700 4900
Wire Wire Line
	2350 5050 2350 4200
Wire Wire Line
	2350 4200 2600 4200
Wire Wire Line
	2350 4200 2350 3350
Wire Wire Line
	2350 3350 2550 3350
Connection ~ 2350 4200
Wire Wire Line
	2350 3350 2350 2500
Wire Wire Line
	2350 2500 2550 2500
Connection ~ 2350 3350
Wire Wire Line
	2350 2500 2350 2300
Connection ~ 2350 2500
Wire Wire Line
	5700 2250 5950 2250
Wire Wire Line
	5950 2250 5950 3100
Wire Wire Line
	5950 3100 5700 3100
Wire Wire Line
	5950 3100 5950 3950
Wire Wire Line
	5950 3950 5750 3950
Connection ~ 5950 3100
Wire Wire Line
	5950 3950 5950 4800
Wire Wire Line
	5950 4800 5750 4800
Connection ~ 5950 3950
Wire Wire Line
	5950 2250 5950 1650
Wire Wire Line
	5950 1650 4200 1650
Connection ~ 5950 2250
Connection ~ 4200 1650
Wire Wire Line
	4200 1650 4200 1400
Wire Wire Line
	2350 5050 3150 5050
Wire Wire Line
	3900 5900 4200 5900
$Comp
L UPSGROUP8-rescue:1N750-1N750 Z?
U 1 1 60BEC690
P 4200 5100
AR Path="/60BEC690" Ref="Z?"  Part="1" 
AR Path="/60BA9BF8/60BEC690" Ref="Z1"  Part="1" 
F 0 "Z1" H 4500 5367 50  0000 C CNN
F 1 "1N750" H 4500 5276 50  0000 C CNN
F 2 "1N750:DIOAD1256W56L444D203" H 4600 5250 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_view/10928-sa5-7-datasheet" H 4600 5150 50  0001 L CNN
F 4 "Zener Diodes Voltage Regulator" H 4600 5050 50  0001 L CNN "Description"
F 5 "" H 4600 4950 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 4600 4850 50  0001 L CNN "Manufacturer_Name"
F 7 "1N750" H 4600 4750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-1N750(DO-35)" H 4600 4650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/1N750?qs=Xwz%252BGKGy4sB1rzIdx4SmVg%3D%3D" H 4600 4550 50  0001 L CNN "Mouser Price/Stock"
F 10 "1N750" H 4600 4450 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/1n750/microsemi" H 4600 4350 50  0001 L CNN "Arrow Price/Stock"
	1    4200 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 5700 4200 5900
Wire Wire Line
	3900 5050 3900 5900
Connection ~ 4200 5900
Wire Wire Line
	9700 4900 9700 5900
Wire Wire Line
	6950 5900 6950 6100
Connection ~ 6950 5900
Wire Wire Line
	6950 5900 9700 5900
Text GLabel 6950 6100 0    50   Input ~ 0
GND
Text GLabel 2350 2300 0    50   Input ~ 0
Vbat
Text HLabel 4200 1400 0    50   Input ~ 0
Pout
NoConn ~ 4500 2150
NoConn ~ 5700 2150
NoConn ~ 5700 2450
NoConn ~ 4500 3000
NoConn ~ 5700 3000
NoConn ~ 5700 3300
NoConn ~ 4550 3850
NoConn ~ 5750 3850
NoConn ~ 5750 4150
NoConn ~ 4550 4700
NoConn ~ 5750 4700
NoConn ~ 5750 5000
Wire Wire Line
	4200 1650 4200 2250
Wire Wire Line
	4200 2250 4500 2250
Connection ~ 4200 2250
Wire Wire Line
	4200 2250 4200 3100
Wire Wire Line
	4200 3100 4500 3100
Connection ~ 4200 3100
Wire Wire Line
	4200 3100 4200 3950
Wire Wire Line
	4550 3950 4200 3950
Connection ~ 4200 3950
Wire Wire Line
	4200 3950 4200 4800
Wire Wire Line
	4200 4800 4550 4800
Connection ~ 4200 4800
Wire Wire Line
	4200 4800 4200 5100
Wire Wire Line
	4500 2450 4400 2450
Wire Wire Line
	4400 2450 4400 3300
Wire Wire Line
	4400 3300 4500 3300
Wire Wire Line
	4400 3300 4400 4150
Wire Wire Line
	4400 4150 4550 4150
Connection ~ 4400 3300
Wire Wire Line
	4400 4150 4400 5000
Wire Wire Line
	4400 5000 4550 5000
Connection ~ 4400 4150
Wire Wire Line
	4400 5000 4400 5900
Wire Wire Line
	4200 5900 4400 5900
Connection ~ 4400 5000
Connection ~ 4400 5900
Wire Wire Line
	4400 5900 6950 5900
$EndSCHEMATC
