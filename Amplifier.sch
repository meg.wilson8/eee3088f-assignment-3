EESchema Schematic File Version 4
LIBS:UPSGROUP8-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Amplifier Circuit"
Date "2021-06-04"
Rev "1"
Comp "UCT Group 9"
Comment1 "Drops supply voltage to 3.3V"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC2
U 1 1 60BFC588
P 4050 3200
F 0 "IC2" H 4650 3465 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 4650 3374 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 5100 3300 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 5100 3200 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 5100 3100 50  0001 L CNN "Description"
F 5 "1.1" H 5100 3000 50  0001 L CNN "Height"
F 6 "Analog Devices" H 5100 2900 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 5100 2800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 5100 2700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 5100 2600 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 5100 2500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 5100 2400 50  0001 L CNN "Arrow Price/Stock"
	1    4050 3200
	1    0    0    -1  
$EndComp
$Comp
L OP1177ARMZ-REEL:OP1177ARMZ-REEL IC3
U 1 1 60BFD8CB
P 6750 3200
F 0 "IC3" H 7350 3465 50  0000 C CNN
F 1 "OP1177ARMZ-REEL" H 7350 3374 50  0000 C CNN
F 2 "OP1177ARMZ-REEL:SOP65P490X110-8N" H 7800 3300 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1597712" H 7800 3200 50  0001 L CNN
F 4 "Precision Low Noise, Low Input Bias Current Operational Amplifiers" H 7800 3100 50  0001 L CNN "Description"
F 5 "1.1" H 7800 3000 50  0001 L CNN "Height"
F 6 "Analog Devices" H 7800 2900 50  0001 L CNN "Manufacturer_Name"
F 7 "OP1177ARMZ-REEL" H 7800 2800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-OP1177ARMZ-R" H 7800 2700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/OP1177ARMZ-REEL?qs=WIvQP4zGaniIfx%252BSC20q%252Bg%3D%3D" H 7800 2600 50  0001 L CNN "Mouser Price/Stock"
F 10 "OP1177ARMZ-REEL" H 7800 2500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/op1177armz-reel/analog-devices" H 7800 2400 50  0001 L CNN "Arrow Price/Stock"
	1    6750 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60BFE58C
P 4650 2450
F 0 "R4" V 4445 2450 50  0000 C CNN
F 1 "3.3k" V 4536 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 4650 2450 50  0001 C CNN
F 3 "~" H 4650 2450 50  0001 C CNN
	1    4650 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R5
U 1 1 60C014C1
P 5900 3300
F 0 "R5" V 5695 3300 50  0000 C CNN
F 1 "10k" V 5786 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 5900 3300 50  0001 C CNN
F 3 "~" H 5900 3300 50  0001 C CNN
	1    5900 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R6
U 1 1 60C01B6C
P 7400 2450
F 0 "R6" V 7195 2450 50  0000 C CNN
F 1 "10k" V 7286 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 7400 2450 50  0001 C CNN
F 3 "~" H 7400 2450 50  0001 C CNN
	1    7400 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	3600 2450 4550 2450
Wire Wire Line
	4750 2450 5600 2450
Wire Wire Line
	5600 3300 5800 3300
Wire Wire Line
	5600 3400 5250 3400
Wire Wire Line
	6000 3300 6450 3300
Wire Wire Line
	6450 3300 6450 2450
Connection ~ 6450 3300
Wire Wire Line
	6450 3300 6750 3300
Wire Wire Line
	8450 2450 8450 3400
Wire Wire Line
	8450 3400 7950 3400
Wire Wire Line
	7500 2450 8450 2450
Wire Wire Line
	6450 2450 7300 2450
Wire Wire Line
	6750 3900 6750 3500
Wire Wire Line
	5100 3900 5100 4250
Wire Wire Line
	5100 3900 6450 3900
Text GLabel 5100 4250 0    50   Input ~ 0
GND
Wire Wire Line
	5600 2450 5600 3300
Connection ~ 5100 3900
Wire Wire Line
	5600 3400 5600 3300
Connection ~ 5600 3300
Wire Wire Line
	4050 3900 5100 3900
$Comp
L Device:R_Small_US R3
U 1 1 60BFFA1C
P 3100 3300
F 0 "R3" V 2895 3300 50  0000 C CNN
F 1 "10k" V 2986 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3100 3300 50  0001 C CNN
F 3 "~" H 3100 3300 50  0001 C CNN
	1    3100 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 3300 3600 3300
Wire Wire Line
	3600 3300 3600 2450
Connection ~ 4050 3900
Wire Wire Line
	3000 3300 2550 3300
Wire Wire Line
	3600 3300 4050 3300
Connection ~ 3600 3300
Wire Wire Line
	4050 3400 3900 3400
Wire Wire Line
	3900 3400 3900 3900
Wire Wire Line
	3900 3900 4050 3900
Wire Wire Line
	6750 3400 6450 3400
Wire Wire Line
	6450 3400 6450 3900
Connection ~ 6450 3900
Wire Wire Line
	6450 3900 6750 3900
Text GLabel 2550 3300 0    50   Input ~ 0
Vbat
Text HLabel 8450 3400 0    50   Input ~ 0
Vout
Text HLabel 7950 3300 0    50   Input ~ 0
Pout
Text HLabel 5250 3300 0    50   Input ~ 0
Pout
NoConn ~ 4050 3200
Wire Wire Line
	4050 3500 4050 3900
NoConn ~ 5250 3500
NoConn ~ 5250 3200
NoConn ~ 6750 3200
NoConn ~ 7950 3200
NoConn ~ 7950 3500
$EndSCHEMATC
