Version 4
SHEET 1 1072 680
WIRE 176 -128 176 -272
WIRE 272 -128 176 -128
WIRE 464 -112 336 -112
WIRE 592 -112 544 -112
WIRE 608 -112 592 -112
WIRE 736 -112 672 -112
WIRE 272 -96 -64 -96
WIRE -192 -48 -192 -272
WIRE -160 -48 -192 -48
WIRE -64 -48 -64 -96
WIRE -64 -48 -80 -48
WIRE -32 -48 -64 -48
WIRE 80 -48 48 -48
WIRE 176 32 176 -128
WIRE 272 32 176 32
WIRE 464 48 336 48
WIRE 592 48 544 48
WIRE 608 48 592 48
WIRE 736 48 736 -112
WIRE 736 48 672 48
WIRE 272 64 -64 64
WIRE -480 96 -480 64
WIRE -336 96 -336 64
WIRE -192 112 -192 -48
WIRE -160 112 -192 112
WIRE -64 112 -64 64
WIRE -64 112 -80 112
WIRE -32 112 -64 112
WIRE 80 112 80 -48
WIRE 80 112 48 112
WIRE 176 192 176 32
WIRE 272 192 176 192
WIRE -480 208 -480 176
WIRE -336 208 -336 176
WIRE 464 208 336 208
WIRE 592 208 544 208
WIRE 608 208 592 208
WIRE 736 208 736 48
WIRE 736 208 672 208
WIRE 272 224 -64 224
WIRE -192 272 -192 112
WIRE -160 272 -192 272
WIRE -64 272 -64 224
WIRE -64 272 -80 272
WIRE -32 272 -64 272
WIRE 80 272 80 112
WIRE 80 272 48 272
WIRE 176 352 176 192
WIRE 272 352 176 352
WIRE 464 368 336 368
WIRE 592 368 544 368
WIRE 608 368 592 368
WIRE 736 368 736 208
WIRE 736 368 672 368
WIRE 272 384 -64 384
WIRE -192 432 -192 272
WIRE -64 432 -64 384
WIRE -64 432 -192 432
WIRE -32 432 -64 432
WIRE 80 432 80 272
WIRE 80 432 48 432
WIRE 176 432 176 352
WIRE 80 528 80 432
WIRE 176 528 176 496
WIRE 736 528 736 368
FLAG -336 64 Vin
FLAG -336 208 0
FLAG 176 528 0
FLAG 80 528 0
FLAG -192 -272 Vin
FLAG -480 64 Vref
FLAG -480 208 0
FLAG 176 -272 Vref
FLAG 304 -80 0
FLAG 304 80 0
FLAG 304 240 0
FLAG 304 400 0
FLAG 304 -144 Vref
FLAG 304 16 Vref
FLAG 304 176 Vref
FLAG 304 336 Vref
FLAG 736 528 0
FLAG 592 368 D1
FLAG 592 208 D2
FLAG 592 48 D3
FLAG 592 -112 D4
SYMBOL res 560 352 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 680
SYMBOL res 560 192 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 680
SYMBOL res 560 32 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 680
SYMBOL res 560 -128 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 680
SYMBOL Misc\\battery -336 80 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 10
SYMBOL zener 192 496 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D5
SYMATTR Value 1N750
SYMBOL res 64 416 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R5
SYMATTR Value 10k
SYMBOL res -64 256 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R6
SYMATTR Value 2k
SYMBOL res 64 256 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R7
SYMATTR Value 8k
SYMBOL res -64 96 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R8
SYMATTR Value 3k
SYMBOL res 64 96 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R9
SYMATTR Value 7k
SYMBOL res -64 -64 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R10
SYMATTR Value 4k
SYMBOL res 64 -64 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R11
SYMATTR Value 6k
SYMATTR SpiceLine tol=1 pwr=0.1
SYMBOL Misc\\battery -480 80 R0
SYMATTR InstName V2
SYMATTR Value 5V
SYMBOL OpAmps\\OP1177 304 304 R0
SYMATTR InstName U1
SYMBOL OpAmps\\OP1177 304 144 R0
SYMATTR InstName U2
SYMBOL OpAmps\\OP1177 304 -16 R0
SYMATTR InstName U3
SYMBOL OpAmps\\OP1177 304 -176 R0
SYMATTR InstName U4
SYMBOL LED 608 384 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D1
SYMATTR Value NSPW500BS
SYMBOL LED 608 224 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D2
SYMATTR Value NSPW500BS
SYMBOL LED 608 64 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D3
SYMATTR Value NSPW500BS
SYMBOL LED 608 -96 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D4
SYMATTR Value NSPW500BS
TEXT -504 280 Left 2 !.tran 0 100 1 0.1
